package com.example.team.multiplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText et1,et2;
    private Button buttonmult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }
    public void addListenerOnButton() {
        et1 = (EditText) findViewById(R.id.editText);
        et2 = (EditText) findViewById(R.id.editText2);
        buttonmult = (Button) findViewById(R.id.button);


    }
    public void onClick(View v) {

        int value1=Integer.parseInt(et1.getText().toString());
        int value2=Integer.parseInt(et2.getText().toString());

        int rel=value1*value2;

        Toast.makeText(this, String.valueOf(rel), Toast.LENGTH_SHORT).show();
    }

}